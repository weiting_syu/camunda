
package com.wistron.example.delegate;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * Page/Class Name: SayHelloDelegate
 * Title:
 * Description:
 * Company:	Wistron Corp.
 * author: louis
 * Create Date:	2018年6月19日
 * Last Modifier: louis
 * Last Modify Date: 2018年6月19日
 * Version 1.0
 *
 */
@Component
public class SayHelloDelegate implements JavaDelegate {
    //================================================
    //== [Enumeration types] Block Start
    //====
    //====
    //== [Enumeration types] Block End
    //================================================
    //== [static variables] Block Start
    //====
    private static final Logger LOGGER = LoggerFactory.getLogger(SayHelloDelegate.class);

    //====
    //== [static variables] Block Stop
    //================================================
    //== [instance variables] Block Start
    //====
    //====
    //== [instance variables] Block Stop
    //================================================
    //== [static Constructor] Block Start
    //====
    //====
    //== [static Constructor] Block Stop
    //================================================
    //== [Constructors] Block Start (含init method)
    //====
    //====
    //== [Constructors] Block Stop
    //================================================
    //== [Static Method] Block Start
    //====
    //====
    //== [Static Method] Block Stop
    //================================================
    //== [Accessor] Block Start
    //====
    //====
    //== [Accessor] Block Stop
    //================================================
    //== [Overrided Method] Block Start (Ex. toString/equals+hashCode)
    //====
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        
        LOGGER.info("hello {}", execution);
    }
    //====
    //== [Overrided Method] Block Stop
    //================================================
    //== [Method] Block Start
    //====
    //####################################################################
    //## [Method] sub-block :
    //####################################################################
    //====
    //== [Method] Block Stop
    //================================================
}
