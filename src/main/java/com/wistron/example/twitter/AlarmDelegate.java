package com.wistron.example.twitter;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

/**
 * Use this delegate instead of TweetContentDelegate, if you don't want to
 * access Twitter, but just to do some sysout.
 */
@Service("alarmAdapter")
public class AlarmDelegate implements JavaDelegate {

	public void execute(DelegateExecution execution) throws Exception {
		System.out.println("\n\n\n######\n\n\n");
		System.out.println("This is alarm --------  ");
		System.out.println("\n\n\n######\n\n\n");
	}

}
