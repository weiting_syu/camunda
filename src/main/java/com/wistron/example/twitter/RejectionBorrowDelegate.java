package com.wistron.example.twitter;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

/**
 * Rejection is just done via a sysout. You could, for example, implement
 * sending mail to the author here. Use your own Mail mechanisms for this or use
 * your application server features.
 */
@Service("messageAdapter")
public class RejectionBorrowDelegate implements JavaDelegate {

	public void execute(DelegateExecution execution) throws Exception {
		String content = (String) execution.getVariable("note");
		String comments = (String) execution.getVariable("comments");
		Boolean approved = (Boolean) execution.getVariable("approved");

		if (!approved) {
			System.out.println("Hi!\n\n" + "Unfortunately your apply has been rejected.\n\n" + "Original content: "
					+ content + "\n\n" + "Comment: " + comments + "\n\n"
					+ "Sorry, please try with better content the next time :-)");
		}
		if (approved) {
			System.out.println("恭禧夫人、賀禧老爺!!");
		}
	}

}
